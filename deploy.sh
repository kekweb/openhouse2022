#! /usr/bin/env bash

echo "STEP1: Remove existing public/"
# rm -r public
echo "STEP2: Build..."
npm run build
echo "STEP3: Upload..."
rsync -auvz --delete public/ kek2:~/openhouse/2022/
echo "Finished !"
