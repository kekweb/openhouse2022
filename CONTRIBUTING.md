# Thank you for contributing to KEK Openhouse 2022 Website

修正依頼などがある場合は、こちらの[ボード](https://gitlab.com/kekweb/openhouse2022/-/issues)に issue を作成してください。

## 修正を提案する場合

- はじめに、issue リストを確認し、同じ内容の修正依頼や議論が行われていないか確認してください
  - すでに議論が進んでいる場合は、そのままコメント欄に参加してください
- 新たに issue を作成する場合は、下記のテンプレートを利用してください
  - `article_new` : [新規記事の作成](.gitlab/issue_templates/article_new.md)
  - `article_fix` : [既存記事の修正](.gitlab/issue_templates/article_fix.md)
  - `bug` : [バグ報告](.gitlab/issue_templates/bug.md)
  - `feature` : [機能追加](.gitlab/issue_templates/feature.md)
  - `fix` : [修正依頼](.gitlab/issue_templates/fix.md)
 - GitLab にアクセスできない場合は、メールでウェブチーム宛に送ってください
  - その際は、上記のテンプレートを参考にしてください
