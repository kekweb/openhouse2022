# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 0.20.0 (2022-09-13)

### Added

- **content/blog/b17.md**: ランキングを公開した
- **layouts/shortcodes/ranking.html**: ランキング表示用のショートコードを作成した
### Fixed

- **layouts/shortcodes/ranking.html**: スコアに単位を追加した

## 0.19.0 (2022-09-08)

### Fix

- **content/cool/e04.md**: 得票数No.1になったことを追記した
- **content/cool/**: 予選の写真も公開した
- **content/cool/e04.md**: 投票結果を追加した
- **content/cool/_index.md**: かっこいいもの選手権の投票フォームのリンクを削除した
- **content/stream/s12.md**: 出演者の画像を追加した
- **content/stream/s12.md**: タイトルを修正した
- **content/covid19/c04.md**: タイトルを変更した

## 0.18.0 (2022-09-05)

### Added

- **content/covid19/c04.md**: 終了後のコロナ対応連絡先を追加した

### Fixed

- **config/mini/config.toml**: 本番ページのメニューにかっこいいもの選手権を追加した
- **config/gitlab/config.toml**: GitLabのメニューにかっこいいもの選手権を追加した
- **config/_default/config.toml**: メニューにかっこいいもの選手権を追加してみた
- **content/stream/_index.md**: OGP画像を変更した

## 0.17.3 (2022-09-03)

### Added

- **content/blog/b16.md**: 記事を公開した
### Fix

- **content/stream/s06.md**: タイトルを修正した

## 0.17.2 (2022-09-03)

### Added

- **content/stream/s01.md**: 「オープニング」を公開した
- **content/cool/_index.md**: 投票フォームを追加した

### Fixed

- **content/stream/s01.md**: 総合司会の写真を追加した
- **content/stream/s13.md**: 結果発表を修正した
- **content/stream/s09.md**: 中間発表を修正した
- **content/stream/s04.md**: 選手紹介を修正した

## 0.17.1 (2022-09-03)

### Added

- **content/stream/s02.md**: 「加速器ってなに？」を公開した
- **content/stream/s03.md**: 「加速器だから見える世界」を公開した
- **content/stream/s04.md**: 「KEKかっこいいもの選手権」を公開した
- **content/stream/s05.md**: 「量子の世界」を公開した
- **content/stream/s06.md**: 「加速器をささえるコンピューター」を公開した
- **content/stream/s07.md**: 「加速器ではたらく超伝導電磁石」を公開した
- **content/stream/s08.md**: 「KEKゆるいクイズ」を公開した
- **content/stream/s10.md**: 「加速器をささえる地面の話」を公開した
- **content/stream/s11.md**: 「ヒッグス粒子発見から10年」を公開した
- **content/stream/s12.md**: 「加速器でこれから見える世界」を公開した

### Fixed

- **config/_default/config.toml**: paginateを増やしてページャが実質表示されないようにした
- **content/stream/s02.md**: 概要の区切り場所を変更した
- **content/stream/s09.md**: 開始時刻を修正した
- **content/stream/s01.md**: オープニングに総合司会の紹介を追加した
- **content/stream/_index.md**: オンライン配信のトップページを修正した
- **content/cool/**: TOP6を公開した

## 0.17.0 (2022-09-02)

### Added

- **content/blog/b15.md**: ショート動画・第11弾を公開した
- **content/blog/b14.md**: ショート動画・第10弾を公開した
- **static/cool/**: かっこいいもの選手権の写真置き場を作成した
- **content/cool/_index.md**: かっこいいもの選手権のセクションを追加した
- **content/cool/**: かっこいいもの選手権のエントリーを追加した

### Fixed

- **content/stream/s04.md**: かっこいいもの選手権を修正した
- **content/stream/**: 開始時刻を修正した

## 0.16.0 (2022-08-31)

### Added

- **content/covid19/countermeasures.pdf**: 感染症対策のPDFを追加した
- **content/pledges/_index.md**: 誓約書と確認書のインデックスを作成した
- **static/pledges/**: 誓約書と確認書のPDFを配置した
- **content/stream/_index.md**: YouTubeを埋め込んだ

### Fix

- **content/tour/flow/reception.md**: 受付手続きの文章を整理した
- **content/tour/flow/**: ツアー→コースに統一した
- **data/features.json**: 受付の〆切日を追記した
- **data/features.json**: ニコニコ生放送を追加した
- **content/stream/_index.md**: ニコニコ生放送のURLを追加した
- **content/covid19/c02.md**: 誓約書・確認書のPDFのリンクを追加した
- **content/stream/_index.md**: YouTubeライブのURLを追記した

## 0.15.0 (2022-08-31)

### Added

- **content/blog/b13.md**: ショート動画・第9弾を公開した
- **content/blog/b12.md**: ショート動画・第8弾を公開した
- **content/blog/b10.md**: ショート動画・第7弾を公開した
- **content/blog/b09.md**: ショート動画・第6弾を公開した
- **content/blog/b11.md**: ショート動画・第5弾を公開した
- **layouts/shortcodes/cast.html**: 出演者を表示するショートコードを作成した

### Fixed

- **content/blog/**: ショート動画の概要を揃えた
- **content/blog/b14.md**: 内容が重複していたので削除した

## 0.14.0 (2022-08-28)

### Added

- **static/img/cast/guest.png**: ゲストのデフォルト画像を追加した
- **content/stream/**: 出演者の写真を追加した
- **static/img/stream/**: カバー画像を追加した

### Fixed

- **layouts/partials/summary.html**: アイコンを時計に変更した
- **layouts/partials/summary.html**: stream用の日付表示を追加した
- **content/stream/**: 出演者にショートコードを使ってみた
- **content/stream/s02.md**: ショートコードを使ってみた
- **content/stream/**: 企画の内容を更新した

## 0.13.0 (2022-08-27)

### Fix

- **content/quiz/_index.md**: 出演者を追加した
- **layouts/shortcodes/peatix.html**: チケット予約の文章を修正した

## 0.12.0 (2022-08-25)

### Fix

- **layouts/partials/recent.html**: 新着情報の表示件数を増やした
- **content/blog/b07.md**: QKの説明を修正した
- **layouts/shortcodes/peatix.html**: 予約枠の説明を修正した
- **config/mini/config.toml**: メニューを追加した

### Feat

- **content/blog/b08.md**: 見学ツアーの申し込みのお礼を追加した

## 0.11.0 (2022-08-23)

### Feat

- **content/blog/b07.md**: 須貝さん出演を公開
- **content/blog/b06.md**: かっこいいもの選手権を公開した
- **content/blog/b07.md**: 須貝さん出演記事の下書き追加した
- **content/blog/b05.md**: ショート動画第4弾を公開した
- **content/blog/b05.md**: ショート動画・第4弾を公開した

### Fix

- **content/blog/b07.md**: 本文でリモート出演を明記した
- **content/tour/t06.md**: 文章を修正した
- **content/blog/b06.md**: ツイートを追加した
- **content/blog/b06.md**: かっこいいもの選手権の記事を作成した
- **content/blog/b05.md**: ショート動画・第4弾を修正した
- **content/blog/b04.md**: ショート動画・第3弾を修正した

## 0.10.0 (2022-08-22)

### Feat

- **content/blog/b04.md**: ショート動画4を公開した

### Fix

- **content/blog/b04.md**: ショート動画4を修正した

## 0.9.0 (2022-08-22)

### Fix

- **layouts/shortcodes/peatix.html**: 追記した時刻を修正
- **layouts/shortcodes/peatix.html**: キャンセル関係の文章を追加した
- **content/tour/t02.md**: ウィジェットの高さを指定した
- **layouts/shortcodes/peatix.html**: デフォルトの高さを調整した
- **content/covid19/c03.md**: 飲める場所の指示を修正した
- **layouts/partials/summary.html**: 詳細を確認するにアイコンを追加した
- **content/tour/flow/_index.md**: ツアー参加の流れを公開した
- **content/tour/_index.md**: 複数予約を遠慮してほしいことを強調した
- **content/covid19/c02.md**: 別紙様式があることを追記した
- **content/tour/_index.md**: 複数予約禁止を追記した
- **content/tour/flow/bus.md**: タイトルを変更した
- **content/tour/flow/visit.md**: リンクを修正した
- **layouts/shortcodes/peatix.html**: 受付開始時刻を追加した
- **content/help/h01.md**: 文章を微修正した
- **config/_default/config.toml**: メニュー名を変更した
- **layouts/partials/summary.html**: 「つづきを読む」を追加した
- **content/help/h01.md**: ご案内とお願いを公開した
- **content/blog/b06.md**: 公開日を修正した
- **content/tour/_index.md**: リンクを追加した

### Feat

- **content/tour/tt.md**: 全コースの時刻表を公開した
- **content/tour/t01.md**: ウィジェットを追加した

## 0.8.0 (2022-08-21)

### Fix

- **content/help/h00.md**: ご案内とお願いを整理した
- **content/help/h00.md**: ご案内とお願いの下書きを追加した
- **content/tour/t05.md**: 誤字を修正した
- **content/help/h01.md**: コンテンツを更新した
- **content/tour/t01.md**: 出発時刻のアイコンを変更した
- **content/tour/**: 出発時刻を追加した
- **content/blog/b06.md**: 下書きモードにした
- **static/img/map/**: png画像を削除した
- **content/tour/t01.md**: 地図のパスを修正した
- **static/img/map/**: 見学の順路を差し替えた
- **config/_default/config.toml**: OGP画像のパスが間違っていたので修正した
- **content/tour/t04.md**: Dコースの写真を更新した
- **static/img/tour/**: Peatixに使った表紙絵を追加した
- **static/img/map/**: 受付の地図を追加した
- **config/gitlab/config.toml**: メニューバーを修正した
- **static/css/custom.css**: strongの文字サイズを調整した
- **content/tour/flow/_index.md**: 当日の流れを分割した
- **content/tour/tt.md**: 全コースの時刻表を追加した
- **content/covid19/_index.md**: 必読を追加した
- **config/_default/config.toml**: メニューに感染症対策を追加した
- **content/covid19/c02.md**: 検温と誓約書を別ファイルにした
- **content/covid19/c03.md**: ファイル名を変更した
- **static/css/custom.css**: 本文中の強調（strong）は文字サイズが本文に揃うようにした
- **content/tour/_index.md**: 来場の前にを追記した
- **content/tour/_index.md**: 感染症・熱中症対策を追記した
- **content/tour/_index.md**: 予約の申し込みを修正した
- **content/tour/_index.md**: 概要を修正した
- **content/tour/_index.md**: 修正した
- **layouts/shortcodes/peatix.html**: パラメータなしで使えるようにした
- **content/stream/**: 修正した
- **layouts/shortcodes/feature.html**: 全体を中央揃えに修正した
- **static/img/peatix.jpg**: Peatixのロゴを追加した
- **content/tour/_index.md**: 予約にPeatixを使うことを追記した
- **content/blog/b03.md**: 小林誠杯の開催決定記事を公開した
- **content/quiz/**: 対象の文言を微修正した
- **content/blog**: 修正した

### Feat

- **content/blog/b06.md**: KEKかっこいいもの選手権を追加した
- **content/blog/b05.md**: ショート動画・第4弾を追加した
- **content/blog/b04.md**: ショート動画・第3弾を追加した
- **content/tour/flow.md**: ツアーの流れを追加した
- **content/covid19/_index.md**: 感染症対策のセクションを追加した
- **static/css/custom.css**: emとstrongのCSSを追加した
- **layouts/shortcodes/peatix.html**: Peatix用のショートコードを追加した
- **content/stream/**: オンライン企画の下書きを追加した
- **content/blog/b03.md**: クイズ大会の記事を新規作成した

## 0.7.0 (2022-08-10)

### Fix

- **content/quiz/q01.md**: ルールを修正した
- **config/mini.toml**: メニューに小林誠杯を追加した
- **conent/quiz/_index.md**: ニコ生のURLを追加した
- **content/quiz/q04.md**: 出題の傾向を公開した
- **content/quiz/q04.md**: カソクキッズのリンクを追加した
- **content/quiz/q04.md**: カソクキッズのリンクを追加した
- **content/quiz/**: サブページを公開した
- **content/quiz/_index.md**: 概要を微修正した
- **content/quiz/_index.md**: タイトルを修正した
- **layouts/partials/summary.html**: サムネイルにボーダー追加した
- **content/quiz/**: 各ページのサムネイルを追加した
- **config.toml**: クイズ小林誠杯を追加した
- **config.toml**: クイズ大会のアイコンをトロフィーに変更した
- **content/quiz**: imagesを追加した
- **content/quiz**: クイズ大会のページを修正した
- **static/img/quiz/**: クイズ大会の画像を追加した
- **static/img/quiz/img_vr02.jpg**: クイズ大会の画像を追加した
- **content/tour/t03.md**: fix typo

### Feat

- **content/quiz**: クイズ大会のページを新規作成した

## 0.6.11 (2022-08-05)

### Fix

- **content/blog/b02.md**: ショート動画・第2弾を公開した
- **content/blog/b02.md**: ショート動画・第2弾の下書きを作成した
- **content/tour/_index.md**: 修正
- **content/tour/_index.md**: 修正
- **CHANGELOG.md**: 変更履歴を整理した

## 0.6.10 (2022-08-03)

### Fix

- **content/tour/_index.md**: 概要を修正した
- **content/stream/_index.md**: fix typo
- **content/stream/s02.md**: 企画02のサンプルを追加した
- **content/stream/_index.md**: 概要を修正した
- **content/stream/s01.md**: 企画のサンプルを修正した
- **content/stream/_index.md**: オンライン配信のトップページを修正した
- **content/tour/**: 車いすNGの文章を修正した

## 0.6.9 (2022-07-29)

### Fix

- **content/tour/**: カテゴリ／タグを整理して公開
- **static/img/tour/d**: Dコースの画像のパスを変更した
- **static/img/tour/a/**: Aコースの画像を変更した
- **content/stream/s01.md**: 出演者の顔写真のサンプルイメージを追加した
- **content/stream/s01.md**: オンライン企画のサンプルを作成した

## 0.6.8 (2022-07-27)

### Fix

- **content/blog/b01.md**: 記事を公開した
- **content/blog/b01.md**: タグを追加した
- **content/blog/b01.md**: 30秒動画→ショート動画に変更した
- **cover/blog/open.md**: imagesの画像パスを修正した
- **static/img/tour/**: 不要な写真を削除した
- **static/img/tour/f/**: Fコースの写真を差し替えた
- **static/img/tour/a/**: Aコースの写真を差し替えた
- **static/img/5Q9A**: 高画質の写真は不要になったので削除した
- **data/slide.json**: トップ画像を軽量化した

## 0.6.7 (2022-07-26)

### Fix

- **layouts/partials/summary.html**: 一覧のカバー画像のパスを修正した
- **static/img/default.jpg**: デフォルトのカバー画像を追加した
- **content/blog/b01.md**: カテゴリを削除した
- **content/blog/b01.md**: 記事を下書きモードにした
- **content/blog/b01.md**: 記事を新規作成した
- **.gitlab/issue_templates/article_**:  記事関係のイシューテンプレートを修正した
- **content/stream/_index.md**: 配信プログラム（仮）を修正した
- **content/blog/poster.md**: ポスターを表示するリンクを追記した
- **layouts/shortcodes/**: ファイルの絶対パス／相対パスを生成するショートコードを作成した
- **static/poster/poster2022.pdf**: PDF情報を修正した
- **static/poster/poster2022.jpeg**: ポスターの置き場所を変更した
- **conten/tour/t04.md**: Dコースのリード文を修正した
- **.gitlab/issue_templates/article.md**: articleのイシューテンプレートを修正した
- **content/tour/t02.md**: 受付の建物を修正した
- **content/stream/_index.md**: 配信プログラム（仮）を追加した
- **content/tour/t02.md**: 誤植を修正した
- **config.toml**: YTのアイコンをFA5に変更した
- **config.toml**: FBのアイコンをFA5に変更した
- **config.toml**: TwitterのアイコンをFA5に変更した
- **config.toml**: Homeのアイコンを追加した
- **config/_default/config.toml**: フッターに機構ウェブのリンクを追加した

## 0.6.6 (2022-07-21)

### Fix

- **content/tour/_index.md**: ツアーのインデックスを修正した
- **content/tour/**: 各コースの内容を修正した
- **static/img/tour/b/**: Bコースの写真を追加した
- **content/tour/**: 各ページのsummaryを設定した
- **content/tour/_index.md**: ツアーのインデックスを微修正した
- **content/tour/_index.md**: ツアーのインデックスを修正した
- **content/tour/**: ページの頭に見学の概要を追記した
- **config/_default/config.toml**: 本文中でFA5が使えるようにした
- **content/tour/t01.md**: 収束でOK
- **content/tour/t04.md**: Dコースの内容を修正した
- **static/img/tour/d/d01.jpeg**: Dコースの写真のサイズを修正した
- **static/img/map/**: 順路の地図を更新した

## 0.6.5 (2022-07-19)

### Fix

- **content/tour/t06.md**: Fコースの順路を追記した
- **content/tour/t05.md**: Eコースの順路を追加した
- **content/tour/t04.md**: Dコースの順路を追記した
- **content/tour/t03.md**: Cコースの順路を追記した
- **content/tour/t02.md**: Bコースの見学順路を追記した
- **content/tour/t01.md**: Aコースの見学順路の説明を追加した
- **content/tour/t06.md**: Fコースの内容を修正した
- **content/tour/t05.md**: Eコースの内容を修正した
- **content/tour/t04.md**: Dコースの内容を修正した
- **content/tour/t03.md**: Cコースの内容を修正した
- **content/tour/t02.md**: Bコースの内容を修正した
- **content/tour/t01.md**: Aコースの写真を追加した
- **content/tour/t01.md**: Aコースの写真を追加した
- **content/tour/t01.md**: Aコースの内容を更新した

## 0.6.4 (2022-07-19)

### Fix

- **static/css/custom.css**: 定義リストのスタイルを追加した
- **static/css/custom.css**: 共通しているmargin/paddingの設定を整理した
- **static/css/custom.css**: codeのスタイルを追加した
- **static/css/custom.css**: 見出しのレベルによってwidthを変更した
- **static/css/custom.css**: リストのスタイルを追加した
- **static/css/custom.css**: blockquoteのスタイルを追加した
- **static/css/custom.css**: 見出しにbox-shadowを追加した
- **content/sample/sample01.md**: CSS調整用のサンプルファイルを整理した

### Refactor

- **content/sample/**: 不要なサンプルファイルを削除した
- **content/sample**: blogセクションに作成したサンプルファイルを移動した

## 0.6.3 (2022-07-17)

### Fix

- **static/css/custom.css**: ページタイトルの行間を広げた
- **layouts/_default/single.html**: リンク（ボタン）のスタイルをトップページと揃えた
- **layouts/_default/single.html**: リンク（ボタン）のスタイルを変更した
- **layouts/_default/single.html**: NextInSectionとPrevInSectionを入れ替えた

## 0.6.2 (2022-07-17)

### Fix

- **content/tour/**: 各コースの地図を中央揃えにした
- **content/tour/**: 各コースの地図を追加した
- **static/img/map/**: 見学ツアーの地図用のディレクトリを作成した
- **static/img/tour/f01.png**: Fコースの写真を追加した
- **static/img/tour/e01.jpg**: Eコースの写真を追加した
- **static/img/tour/d01.jpg**: Dコースの写真を追加した
- **static/img/tour/c01.jpeg**: Cコースに提供された画像を追加した
- **static/img/tour/b01.png**: Bコースに提供された画像を追加した
- **static/img/tour/a01.png**: Aコースに提供された画像を追加した
- **static/img/tour/cover.jpeg**: ツアーのカバー画像のファイル名を変更した
- **content/tour/t01.md**: 日時を修正した
- **content/tour/t06.md**: Fコースの下書き（写真なし）を追加した
- **content/tour/t05.md**: Eコースの下書き（写真なし）を作成した
- **content/tour/t04.md**: Dコースの下書き（写真は適当）を追加した
- **content/tour/03.md**: Cコースの下書き（写真なし）を追加した
- **config/gitlab/config.toml**: GitLab Pagesで下書きをビルドするようにした
- **content/tour/t02.md**: Bコースの下書き（写真なし）を追加した
- **content/tour/t01.md**: Aコースの下書き（写真なし）を作成した
- **layouts/partials/header.html**: トップにサイト名を表示した

## 0.6.1 (2022-07-07)

### Fix

- **deploy.sh**: 実行時のメッセージを追加した
- **layouts/partials/slide.html**: 角を丸めた
- **layouts/partials/slide.html**: lgサイズの設定を追加した
- **layouts/partials/slide.html**: mdサイズの設定を追加した
- **layouts/partials/slide.html**: smサイズの設定を追加した
- **layouts/partials/slide.html**: デフォルト（sm以下）のスタイルを設定した

## 0.6.0 (2022-07-04)

### Fix

- **deploy.sh**: www2にデプロイするスクリプトを作成した
- **data/features.json**: 曜日を追加した
- **static/css/custom.css**: 本文中のリンクの色を設定した
- **layouts/_default/single.html**: アイキャッチ画像を中央寄せにした
- **content/blog/poster.md**: ポスター完成のお知らせを作成した
- **config/_default/config.toml**: 設定を微修正した
- **content/tour/_index.md**: 中止の可能性について追記した
- **content/stream/_index.md**: 未定のオンラインコンテンツについて削除した
- **config/gitlab/config.toml**: 公開用にGitLab用の設定を修正した
- **alerts.json**: 表示しないためにファイル名を変更した
- featureのショートコードに変更した
- **config/mini/config.toml**: メニューを追加した
- **static/css/custom.css**: figcaptionのスタイルを追加した
- **config/mini/config.toml**: メニューを減らした
- **layouts/partials/recent.html**: 新着情報のタイトルにパディングを追加した
- **content/tour/_index.md**: 現地ツアーの概要を修正した
- **static/css/custom.css**: 見出しのCSSを修正した
- **layouts/partials/strip.html**: ボタンに黒い枠線を追加した
- **data/features.json**: ニコニコの予定をひとまず削除した
- **content/stream/_index.md**: オンライン配信の概要を修正した
- **content/tour/_index.md**: 現地開催ツアーの概要を追加した
- **static/css/custom.css**: 見出しのマージンを変更した
- **static/css/custom.css**: hrに上下マージンを追加した
- **static/img/tour.jpeg**: 見学の画像を追加した
- **content/stream/_index.md**: オンライン配信のインデックスを修正した
- **static/css/custom.css**: 仮で対処
- **config/_default/config.toml**: ライブ配信→オンライン配信に変更した
- **content/stream/_index.md**: 下書きを追加した
- **static/img/stream.jpeg**: オンライン配信のインデックス画像を追加した
- **static/img/logo/KEKLogo01w.png**: ロゴを小さくした
- **content/stream**: stream / tour のインデックスに画像を追加した
- **config/_default/config.toml**: buildFuture = trueにした
- **data/features.json**: タイトルを修正した
- **config/_default/config.toml**: メニュー名のアイコンを変更した
- **data/features.json**: ボタンのタイトルを変更した
- **layouts/_default/list.html**: アイコンを表示できるようにした
- **content/**: それぞれのセクションの_index.mdにアイコンを追加した
- **layouts/_default/single.html**: ページのナビゲーションを整理しようとした
- **layouts/_default/single.html**: share_buttonを部分テンプレートとして分離した
- **layouts/partials/summary.html**: サムネイルのブレークポイントをlg->mdに変更した
- **layouts/partials/summary.html**: params.image を使えないようにした
- **content/**: image -> images に変更した
- **layouts/partials/summary.html**: params.imagesを使えるようにした
- **layouts/_default/single.html**: meta_tagsを削除した
- **content/blog/open.md**: params.imagesを追加した
- **layouts/index.html**: scriptを部分テンプレートとして分離した
- **layouts/_default/single.html**: disqusを削除した

### Feat

- **layouts/shortcodes/feature.html**: 画像を相対パスで挿入するショートコードを作成した
- **layouts/_default/single.html**: params.imagesを使えるようにした

## 0.5.0 (2022-06-28)

### Fix

- **content/blog/open.md**: サイトオープンのお知らせ（ダミー）を追加した
- **config/mini/config.toml**: 最小限だけ公開するための設定ファイルを用意した
- **config/_default/config.toml**: defaultのbaseURLを変更した
- **config/gitlab/config.toml**: GitLab CI用の設定を追加した
- **config/_default/config.toml**: 設定ファイルのパスを変更した
- **static/img/logo/KEKLogo01w.png**: ロゴを白抜きに変更した
- **layouts/partials/header.html**: ナビゲーションの色をチラシと同じ色にした
- **themes/tella/assets/css/style.css**: 自動生成されたCSSを更新した
- **content/blog/_index.md**: 新着情報のトップページのタイトルを修正した
- **config.toml**: recent.htmlで表示するセクションを指定した
- **layouts/partials/recent.html**: indexと中途半端に混じっていたので整理した
- **layouts/partials/recent.html**: テーマからコピペした
- **config.toml**: 現地開催ツアー→現地見学ツアーに修正した
- **config.toml**: メニュー名を修正した
- **layouts/_default/list.html**: mainSectionを定義しなくても、一覧が表示できるようにした
- **content/**: サンプルを追加した
- **content/tour/_index.md**: 追加するコンテンツの内容をメモした
- **layouts/partials/summary.html**: サムネイルの高さを増やした
- **layouts/partials/strip.html**: featuredエリアのデータ形式と表示を変更した
- **data/features.json**: 入り口は2つにする
- **config.toml**: stream と tour をメニューに追加した
- **content/help/_index.md**: FAQのインデックスを修正した
- **layouts/_default/single.html**: デバッグ用の表示を削除した
- **content/**: 全てのページをdraftにした
- **layouts/partials/summary.html**: ページ一覧でもblogのときだけ日時を表示するようにした
- **layouts/partials/summary.html**: draftにアイコンを追加した
- **layouts/_default/list.html**: 使わないことにした meta_tags を削除した
- **layouts/_default/single.html**: .Typeによる分岐ができるようになった
- **layouts/_default/single.html**: アイコンを変更した
- **layouts/_default/single.html**: タグも表示することにした
- **layouts/_default/single.html**: author表示は不要なので削除した
- **layouts/_default/single.html**: デバッグ用のラインを追加した
- **themes/tella/layouts/blog/single.html**: 使いたいテンプレートと干渉していたので削除した

## 0.14.0 (2022-08-29)

### Added

- **layouts/shortcodes/cast.html**: 出演者を表示するショートコードを作成した
- **layouts/partials/summary.html**: オンライン企画の一覧に時刻表示を追加した
- **static/img/stream/**: オンライン企画のカバー画像を追加した
- **static/img/cast/**: 出演者の画像を追加した
- **content/stream/**: 出演者の表示を追加した

### Fixed

- **content/stream/**: オンライン企画の下書きを更新した

## 0.13.0 (2022-08-27)

### Added

- **content/stream/**: オンライン企画の下書きを追加した
- **content/quiz/_index.md**: 出演者を追加した

### Fixed

- **layouts/shortcodes/peatix.html**: チケット予約の文章を修正した

## 0.12.0 (2022-08-25)

### Added

- **content/blog/b08.md**: 見学ツアーの申し込みのお礼を追加した
- **config/mini/config.toml**: メニューを追加した

### Fixed

- **layouts/partials/recent.html**: 新着情報の表示件数を増やした
- **content/blog/b07.md**: QKの説明を修正した
- **layouts/shortcodes/peatix.html**: 予約枠の説明を修正した

## 0.11.0 (2022-08-23)

### Feat

- **content/blog/b07.md**: 須貝さん出演を公開
- **content/blog/b06.md**: かっこいいもの選手権を公開した
- **content/blog/b05.md**: ショート動画・第4弾を公開した

### Fix

- **content/blog/b07.md**: 本文でリモート出演を明記した
- **content/blog/b06.md**: ツイートを追加した
- **content/blog/b05.md**: ショート動画・第4弾を修正した
- **content/blog/b04.md**: ショート動画・第3弾を修正した
- **content/tour/t06.md**: 文章を修正した

## 0.10.0 (2022-08-22)

### Feat

- **content/blog/b04.md**: ショート動画・第4弾を追加した

## 0.9.0 (2022-08-22)

### Fix

- **content/tour/_index.md**: 複数予約を遠慮してほしいことを強調した
- **content/tour/flow/visit.md**: リンクを修正した
- **content/tour/flow/bus.md**: タイトルを変更した
- **content/tour/t02.md**: ウィジェットの高さを指定した
- **content/covid19/c02.md**: 別紙様式があることを追記した
- **content/covid19/c03.md**: 飲める場所の指示を修正した
- **layouts/partials/summary.html**: 詳細を確認するにアイコンを追加した
- **layouts/shortcodes/peatix.html**
  - デフォルトの高さを調整した
  - 受付開始時刻を追加した
  - キャンセル関係の文章を追加した
- **config/_default/config.toml**: メニュー名を変更した

### Feat

- **content/help/_index.md**: ご案内とお願いを公開した
- **content/tour/tt.md**: 全コースの時刻表を公開した
- **content/tour/flow/_index.md**: ツアー参加の流れを公開した
- **content/tour/t01.md**: ウィジェットを追加した

## 0.8.0 (2022-08-21)

### Fix

- **config/_default/config.toml**: OGP画像のパスが間違っていたので修正した
- **config/_default/config.toml**: メニューに感染症対策を追加した
- **config/gitlab/config.toml**: メニューバーを修正した
- **content/help/h01.md**: コンテンツを更新した
- **content/tour/**: 出発時刻を追加した
- **content/tour/_index.md**: 整理した
- **content/tour/t01.md**: 地図のパスを修正した
- **content/tour/t01.md**: 出発時刻のアイコンを変更した
- **content/tour/t04.md**: Dコースの写真を更新した
- **content/tour/t05.md**: 誤字を修正した
- **content/tour/flow/_index.md**: 当日の流れを分割した
- **content/tour/tt.md**: 全コースの時刻表を追加した
- **content/covid19/_index.md**: 必読を追加した
- **content/covid19/c02.md**: 検温と誓約書を別ファイルにした
- **content/covid19/c03.md**: ファイル名を変更した
- **static/img/tour/**: Peatixに使った表紙絵を追加した
- **static/img/map/**: png画像を削除した
- **static/img/map/**: 見学の順路を差し替えた
- **static/img/map/**: 受付の地図を追加した
- **static/css/custom.css**: strongの文字サイズを調整した
- **static/css/custom.css**: 本文中の強調（strong）の文字サイズが揃うようにした
- **static/img/peatix.jpg**: Peatixのロゴを追加した
- **layouts/shortcodes/peatix.html**: パラメーターなしで使えるようにした
- **layouts/shortcodes/feature.html**: 全体を中央揃えに修正した

### 公開

- **content/blog/b03.md**: 小林誠杯の開催決定記事を公開した

### 下書き

- **content/blog/b06.md**: KEKかっこいいもの選手権を追加した
- **content/blog/b05.md**: ショート動画・第4弾を追加した
- **content/blog/b04.md**: ショート動画・第3弾を追加した
- **content/tour/flow.md**: ツアーの流れを追加した

### Feat

- **content/covid19/_index.md**: 感染症対策のセクションを追加した
- **static/css/custom.css**: emとstrongのCSSを追加した
- **layouts/shortcodes/peatix.html**: Peatix用のショートコードを追加した

## 0.7.0 (2022-08-10)

### Feat

- **content/quiz/**
  - クイズ大会・小林誠杯のページを新規作成した
  - クイズ大会のアイコンをトロフィーにした
  - ページを公開した
  - ニコ生のURLを追加した
- **config/mini.toml**
  - メニューに小林誠杯を追加した

### Fix

- **layouts/partials/summary.html**
  - サムネイルにボーダー追加した
- **static/img/quiz/**
  - クイズ大会のページで使う画像を追加した

## 0.6.11 (2022-08-05)

### Contents

- **content/blog/b02.md**
  - ショート動画・第2弾を公開した

### Fix

- **content/tour/_index.md**
  - 文章を微修正した

## 0.6.10 (2022-08-03)

### Contents

- **content/tour/_index.md**:
  - 概要を修正した
- **content/stream/_index.md**
  - 概要を修正した
  - 配信プログラムのリストを追加した
- **content/tour/**:
  - 車いすNGの文章を修正した

## 0.6.9 (2022-07-29)

### Contents

- **content/tour/**
  - カテゴリ／タグを整理して公開
- **static/img/tour/**
  - Aコースの写真を（一部）差し替えた
  - Fコースの写真を差し替えた
  - 不要な写真を整理した

### Fix

- **content/stream/s01.md**
  - オンライン企画のサンプルを作成した
  - 出演者の顔写真のサンプルイメージを追加した
- **data/slide.json**
  - トップ画像を軽量化した

## 0.6.8 (2022-07-27)

### Contents

- **content/blog/b01.md**
  - タイトルを修正した
  - タグを追加した
  - 公開した

## 0.6.7 (2022-07-26)

### Contents

- **content/blog/poster.md**
  - ポスターを表示するリンクを追記した
- **content/stream/_index.md**
  - 配信プログラム（仮）を修正した
- **content/tour/t04.md**
  - Dコースのリード文を修正した
- **content/tour/t02.md**
  - Bコースの誤植を修正した

### Fix

- **layouts/partials/summary.html**
  - 一覧のカバー画像のパスを修正した
- **static/img/default.jpg**
  - デフォルトのカバー画像を追加した
- **.gitlab/issue_templates/article_**
  - 記事関係のイシューテンプレートを修正した
- **layouts/shortcodes/**
  - ファイルの絶対パス／相対パスを生成するショートコードを作成した
- **static/poster/**
  - ポスターの置き場所を変更した
  - PDF情報を修正した
- **config/_default/config.toml**
  - フッターに機構ウェブのリンクを追加した
  - Homeのアイコンを追加した
  - TwitterのアイコンをFA5に変更した
  - FBのアイコンをFA5に変更した
  - YTのアイコンをFA5に変更した

## 0.6.6 (2022-07-21)

### Fix

- **content/tour/_index.md**
  - 現地開催ツアーの概要を修正した
- **content/tour/**
  - 各コースの内容を修正した
  - 各ページに .Summary を設定した
- **static/img/tour/b/**
  - Bコースの写真を追加した
- **config/_default/config.toml**
  - 本文中でFA5アイコンを使えるようにした
- **static/img/tour/d/d01.jpeg**
  - Dコースの写真のサイズを修正した
- **static/img/map/**
  - 順路の地図を更新した

## 0.6.5 (2022-07-19)

### Fix

- **content/tour/t06.md**
  - Fコースの内容を修正した
  - Fコースの写真を修正した
  - Fコースの順路を追記した
- **content/tour/t05.md**
  - Eコースの内容を修正した
  - Eコースの写真を修正した
  - Eコースの順路を追記した
- **content/tour/t04.md**
  - Dコースの内容を修正した
  - Dコースの写真を修正した
  - Dコースの順路を追記した
- **content/tour/t03.md**
  - Cコースの内容を修正した
  - Cコースの写真を修正した
  - Cコースの順路を追記した
- **content/tour/t02.md**
  - Bコースの内容を修正した
  - Bコースの写真を修正した
  - Bコースの順路を追記した
- **content/tour/t01.md**
  - Aコースの内容を更新した
  - Aコースの写真を修正した
  - Aコースの順路を追記した

## 0.6.4 (2022-07-19)

### Fix

- **static/css/custom.css**:
  - 見出しのスタイルを変更した
    - 見出しのレベルによってwidthを変更した
    - box-shadowを追加した
  - 共通しているmargin/paddingの設定を整理した
  - 定義リストのスタイルを追加した
  - codeのスタイルを追加した
  - リストのスタイルを追加した
  - blockquoteのスタイルを追加した
- **content/sample/sample01.md**:
  - CSS調整用のサンプルファイルを整理した

### Refactor

- **content/sample/**: 不要なサンプルファイルを削除した
- **content/sample**: blogセクションに作成したサンプルファイルを移動した

## 0.6.3 (2022-07-17)

### Fix

- **static/css/custom.css**:
  - ページタイトルの行間を広げた
- **layouts/_default/single.html**:
  - 次のページ（NextInSection）と前のページ（PrevInSection）の配置を入れ替えた
  - リンク（ボタン）のスタイルをトップページと揃えた
  - 下書きのときにアイコン表示を追加した

## 0.6.2 (2022-07-17)

### Fix

- **content/tour/**:
  - Aコースの下書きを作成した
  - Bコースの下書きを作成した
  - Cコースの下書きを作成した
  - Dコースの下書きを作成した
  - Eコースの下書きを作成した
  - Fコースの下書きを作成した
  - 各コースの地図を追加した
  - 各コースの地図を中央揃えにした
- **static/img/map/**:
  - 見学ツアーの地図用のディレクトリを作成した
- **static/img/tour/cover.jpeg**:
  - ツアーのカバー画像のファイル名を変更した
- **config/gitlab/config.toml**:
  - GitLab Pagesで下書きをビルドするようにした
- **layouts/partials/header.html**:
  - トップにサイト名を表示した

## 0.6.1 (2022-07-07)

### Fix

- **deploy.sh**: 実行時のメッセージを追加した
- **layouts/partials/slide.html**:
  - 6915da343f5c11510a3183864e75595c2804746e : lgサイズの設定を追加した
  - d44dcd2a7bd17fa897a85fa50bfa4b69594e3dd4 : mdサイズの設定を追加した
  - 8df54d5012e09698e547a22521a11a11108a97c9 : smサイズの設定を追加した
  - f19fa90085bdc30d2a790312029c81f2df56569e : デフォルト（sm以下）のスタイルを設定した

## 0.6.0 (2022-07-04)

### Feat

- **deploy.sh**: www2にデプロイするスクリプト
  - 5799eae3cfaf79f134bc11cb66ebbb0f5281e38e : 新規作成した
- **layouts/shortcodes/feature.html**: 画像を相対パスで挿入できるショートコード
  - 7c10bec8a6038efd6ec63e48f7468633ef1a7e1e : デフォルトの``figure``を改良して、相対パスを扱えるようにした
- **layouts/partials/share_button**
  - 561fcb22a08cfad22fe907b77c24088eadc2e85c : シェアボタンを部分テンプレートとして分離した
- **static/css/custom.css**: テーマに付属しているはずのCSSが全然適用されないので、仮で作成した
  - 本部中の見出しのスタイルを追加した
  - 本部中の水平線のスタイルを追加した
  - 本部中の画像（``figure`` / ``figcaption``）のスタイルを追加した
  - 本部中のリンクのスタイルを追加した

### Fix

- **static/img/**: 画像を追加した
  - **tour.jpeg**: 現地見学ツアーの画像
  - **stream.jpeg**: オンライン配信の画像
  - **logo/KEKLogo01w.png**: ロゴを小さくした
- **config**: 設定ファイルをいろいろ変更した
  - **_default/config.toml** : 企画コンテンツは開催日（=未来の日付）で公開すると思うので``buildFuture = true``にしておいた
  - **gitlab/config.toml**: GitLab Pagesでテストページをビルドするための設定
  - **mini/config.toml**: トップページだけをビルドするミニマムな設定
- **data/features.json**:
  - c35fc916d7fa688058dd7208d11531a8e1c0db35 : ボタンのタイトルを変更した
  - a23e0752712ff6ed1c5276e84fa592095c45838c : ニコ生の予定はいったん削除した
- **layouts/index.html**: scriptを部分テンプレートとして分離した
- **layouts/_default/single.html**
  - 3349594002082ec0ae26af8be92d59e532b97798 アイキャッチ画像を中央寄せにした
  - 508a48e3287abdcee05d6de7e9edb499af79b9c0 : フロントマターの``images = []``をアイキャッチ画像として使えるようにした
  - 6bc27a4c5c64c95030fe2ac5cba63f34bc4e9f0e : サムネイルのブレークポイントを``lg``→``md``に変更した
  - disqusは使わないので削除した
  - meta_tagsは使わないので削除した
- **layouts/partials/summary.html**
  - c7d3b8fe517107d8f5b9799e702c88e7b8e095f3 : フロントマターの``images = []``をサムネイル画像として使えるようにした
  - e4510ed3b616ec9138dab5902388769c2e31a100 : テーマにあった``image``は削除した
- **layouts/partials/strip.html**: ボタンに黒い枠線を追加した
- **content/**: コンテンツを追加した
  - **blog/poster.md**: ポスター完成のお知らせを追加した
  - **stream/_index.md**: オンライン配信の概要を追加した
  - **tour/_index.md**: 現地開催ツアーの概要を追加した

## 0.5.0 (2022-06-28)

### Fix

- **config/**: 設定ファイルを整理した
  - 4d067bf7c49cf59730c00bc2bae3c428a19a84bc : トップを先行公開するための設定ファイルを追加した
  - 1eedcc354047c36b9da58af40d363155f01592e6 : GitLab Pages に公開するための設定ファイルを追加した
  - 85beaa91c2f06a41d6949ffe7b674f48ff028c22 : www2 に公開するための設定ファイルをデフォルトに変更した
- **static/img/logo/KEKLogo01w.png**
  - c9eb234011074f798269ad0171f94c4a099ecd01 : テーマカラーに合わせて白抜きロゴに変更した
- **layouts/partials/header.html**: ナビゲーションの部分テンプレートを修正した
  - 575db0130a12ae88380879b4853fdaebfc020cab : テーマカラーをチラシの色に合わせた
  - b5af49cbb709ce8fcdb30642f3f62b4c8e38e39d : メニューにアイコンを追加した
- **layouts/partials/recent.html**: 新着情報の部分テンプレートを整理した
  - 9a52045ed4acdf702f236744403dd0289a5b2f7d : インデックステンプレートと分離した
  - f8864e4a58f96b905f96f3db94d750f32d2545e4 : 表示する `mainSections` を修正した
- **layouts/partials/strip.html**: featured エリアのデータ形式と表示を変更した
  - ca812e99dd29c7332cce71363c2065a4647a99d5 : ライブ配信と現地見学ツアーの入り口を配置した
- **layouts/partials/summary.html**: ページ一覧の部分テンプレートを整理した
  - 929c8b628d4331248adefabce19c1e894e2775c5 : サムネイルの高さを大きくした
  - ed95b1775376a18064722659b698ec4ca4dbd103 : 下書きモードにアイコンを追加した
  - 629b89f2e162cb813dfe7d68c80566ce35f52846 : `blog`セクションのみページ一覧で日付が表示されるように修正した
- **layouts/\_default/list.html**: デフォルトのリストテンプレートを整理した
  - f3fd5588e6c2eb9d1a4eb6e6bc9aa254fe37faf8 : `mainSections`を定義しなくても、各セクションの一覧を表示できるように修正した
  - e4e9b9c8614497647d1b51672fd8ddb86e8ca027 : もう使わない `mata_tags` を削除した
- **layouts/\_default/single.html**: デフォルトのページテンプレートを整理した
  - f838941f5881113064ad86459ebe267e6df9bd0a : `author`表示は削除した
  - c0a2bc40080a9ddaef6ede844f6e2882591a0005 : `tag`表示ができるようにした
  - 876c9eb407b05acd12b3c806969f4be39f2d135f : カテゴリのアイコンを変更した
- **content/**: コンテンツを整理した
  - d7e880d2ef60c5ad55d62bb58e52e8907438a236 : ライブ配信と現地見学ツアー用のセクションを作成した

### Deprecated

- **themes/tella/layouts/blog/single.html**: 使いたいテンプレートと干渉していたので削除した

## 0.4.0 (2022-06-25)

### Fix

- **config.toml**: params.title を追加した
- **config.toml**: twitter の設定を追加した
- **layouts/\_default/baseof.html**: meta_tags のブロックを削除した
- **layouts/partials/head.html**: ビルトインのテンプレを追加した
- **layouts/index.html**: メタタグを削除した
- **layouts/index.html**: テーマからコピペした
- **layouts/partials/footer.html**: 背景色を新しいカラーパレット（primary）に変更した
- **tailwind.config.js**: カラーパレット（primary）を追加した
- **static/css/custom.css**: ハンバーガーメニューの背景色を変更した
- **layouts/partials/head.html**: テーマからコピペした
- **layouts/partials/header.html**: テーマからコピーした
- **data/features.json**: featured の内容を修正した
- **data/alert.json**: 掲載情報を変更した
- **data/featured.json**: オンラインの開催日を追加した
- **content/help/**: すべてのサンプルを draft = true にした

### Feat

- **package.json**: preview モードを追加した
- **layouts/\_default**: タイトルに draft を追加した

## 0.3.0 (2022-06-20)

### Fix

- **layouts/partials/footer.html**: copyright 用のパラメーターを追加した
- **layouts/partials/footer.html**: テーマからコピペした
- **layouts/\_default/single.html**: カテゴリ名の前にハッシュタグを追加した
- **layouts/partials/summary.html**: カテゴリを表示できるようにした
- **layouts/partials/summary.html**: テーマからコピペした
- **content/page/**: サンプルページを修正した
- **content/page/p01.md**: 企画ページのファイル名を変更した
- **layouts/partials/strip.html**: テーマからコピペした
- **layouts/partials/alert.html**: 日付でソート（降順）を追加した
- **layouts/partials/alert.html**: 日付周りのマージンを追加した
- **layouts/partials/alert.html**: パスを間違えていたので修正した
- **data/alert.json**: alert のサンプルを修正した
- **content/blog/markdown.md**: サンプルを修正した
- **content/blog/**: 不要なサンプルを削除した
- **content/blog/sample02.md**: サンプルを修正した
- **content/blog/sample02.md**: サンプルのファイル名を変更した
- **content/blog/sample01.md**: サンプルを修正した
- **content/blog/sample01.md**: サンプルのファイル名を変更した
- **placeholder-text.md**: image を追加した
- **content/blog/placeholder-text.md**: サンプルを作成した

### Feat

- **layouts/alert.html**: テーマからコピペした

## 0.2.0 (2022-06-19)

### Feat

- **content/blog/**: post から blog にパスを変更した
- **content/help/**: ご案内とお願いのテストページを追加した
- **layouts/\_default/list.html**: テーマからテンプレートをコピペした
- **layouts/\_default/single.html**: blog のテンプレートをデフォルトにする
- **static/img/logo/**: ロゴ画像（PNG）を追加した
- **static/img/logo/**: ロゴ画像（PNG）を追加した
- **content/help/**: ヘルプページを追加した

### Fix

- **content/about.md**: about のパスを変更した
- **config.toml**: メニューの順番を変更した
- **content/page/about.md**: image を追加した
- **config.toml**: mainSections に追加した
- **themes/tella/assets/css/style.css**: 更新された CSS を追加した
- **data/alert.json**: alert のサンプルを追加した
- **themes/tella/layouts/\_default/single.html**: .Params.image を表示できるようにした
- **themes/tella/layouts/\_default/single.html**: .Params.image が表示できるように修正中
- **content/help/\_index.md**: デバッグ的な内容を追加した
- **config.toml**: メニューを修正した
- **themes/tella/layouts/partials/recent.html**: All Posts のリンク先を修正した
- **content/help/**: 2019 サイトから内容を転記中
- **content/post/markdown-syntax.md**: 記事を微修正
- **content/post/markdown-syntax.md**: 記事を微修正

### Refactor

- **themes/tella/**: 一度リセットした

## 0.1.0 (2022-06-13)

### Fix

- **themes/tella/assets/css/style.css**: v3.0.4 -> v3.11 の変更点（だと思う）を確認した
- **content/post/**: 記事のフロントマターにサンプル画像（image）を追加した
- **content/post/**: テーマ（tella）のサンプルをまるっとコピーした
- **content/post/\_index.md**: 記事一覧のタイトルを追加した
- **data**: トップページ中断で読み込んでいる JSON データを整理した
- **content/page/sample01.md**: 固定ページのサンプルを追加した
- **content/post/2017-03-07-bigimg-sample.md**: 記事ページのサンプルを修正した
- **static/img**: サンプル画像を追加した
- **content/page/about.md**: 過去のページを参考に about.md を修正した
- **content/post/2017-03-20-photoswipe-gallery-sample.md**: draft に変更した

### Feat

- **tailwind.config.js**: TailwindCSS の設定ファイルを作成した
- **config.toml**: テーマの初期設定を行なった
- **themes/tella**: テーマをインストールした
- **.editorconfig**: .editorconfig を追加した
- **.gitignore**: .gitignore を追加した（macOS と Node 関係のファイル）
