![Build Status](https://img.shields.io/gitlab/pipeline/kekweb/openhouse2022?style=for-the-badge)

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

**目次** （[DocToc](https://github.com/thlorenz/doctoc) による自動生成）

- [KEK 一般公開 2022・ウェブチーム](#kek-一般公開-2022ウェブチーム)
  - [メンバー](#メンバー)
  - [公開ページ](#公開ページ)
  - [テストページ](#テストページ)
  - [リポジトリ](#リポジトリ)
- [ウェブ上での編集作業](#ウェブ上での編集作業)
  - [編集内容の自動テスト](#編集内容の自動テスト)
- [ローカル環境での編集フロー](#ローカル環境での編集フロー)
  - [ローカルサーバを起動する（`$ npm run start`）](#ローカルサーバを起動する-npm-run-start)
  - [リポジトリに変更がないか確認する（`$ git pull`）](#リポジトリに変更がないか確認する-git-pull)
  - [ファイルを編集する](#ファイルを編集する)
  - [変更履歴を作成する（`$cz c`）](#変更履歴を作成するcz-c)
  - [リポジトリに反映させる（`$ git push`）](#リポジトリに反映させる-git-push)
- [ローカル環境の構築](#ローカル環境の構築)
  - [構築ツールをインストールする](#構築ツールをインストールする)
  - [リポジトリをクローンする](#リポジトリをクローンする)
  - [関連パッケージをインストールする](#関連パッケージをインストールする)
- [参考リファレンス](#参考リファレンス)
- [色見本](#色見本)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

---

# KEK 一般公開 2022・ウェブチーム

## メンバー

- 人数 : 6 名
- 権限 : Developer

## 公開ページ

- www2 : https://www2.kek.jp/openhouse/2022/
- アクセス権限 : 制限なし

## テストページ

- GitLab Pages : https://kekweb.gitlab.io/openhouse2022/
- アクセス権限：アクセスできるひとすべて（2022/06/29 に変更）
- 修正などを提案する手順は [CONTRIBUTING](CONTRIBUTING.md) をお読みください

## リポジトリ

- GitLab Repos : https://gitlab.com/kekweb/openhouse2022/
- アクセス権限：アクセスできるひとすべて（2022/06/29 に変更）

---

# ウェブ上での編集作業

- ブラウザ上で操作できるエディターを利用する
  - ブラウザ上で該当ファイルを選択し `Web IDEで編集`する
- 修正が完了したら、編集した内容に説明する簡単なコメントをつけて`コミット`する
  - `コミット` = 「ファイルを保存」に相当する操作

> テキストの修正（例：企画内容の文章）であれば、ウェブ上で編集作業で十分
>
> レイアウトの調整などは、ちょっと複雑な編集が必要なので、後述するローカル環境での編集方法を参照

## 編集内容の自動テスト

- 任意のブランチに変更があると自動テストが実行（GitLab CI を利用）
- 編集をコミットした後の状態は [パイプライン](https://gitlab.com/kekweb/openhouse2022/-/pipelines) の一覧で確認可能
- もし、パイプラインが失敗している場合は、いったん編集の手を止めて修正に専念する
- 分からないことがあれば、すぐにプロジェクトメンバーにヘルプを求める
- デフォルトブランチ（`master`）に変更を加えた場合のみ、テストページが公開される

---

# ローカル環境での編集フロー

1. リポジトリの変更をダウンロードする
2. ローカルサーバーを起動する（Live Reload あり）
3. 該当ファイルを編集する（任意のエディター）
4. 変更をコミット（`commitizen`）
5. リポジトリに変更をアップロードする

編集フロー全体はちょっと複雑かもしれないが、それぞれの作業単位は（それほど）難しくない（はず）

> ローカル環境の構築については後述

## ローカルサーバを起動する（`$ npm run start`）

- 変更点を確認するためのローカルサーバーを立ち上げる
- ライブリロード機能が付いているので、リアルタイムで変更が反映される

```bash
$ npm run start

# ...
# いろいろと表示される途中にサーバのアドレスも表示される
# デフォルトだと http://localhost:1313/openhouse2022/ になっているはず
#...
```

## リポジトリに変更がないか確認する（`$ git pull`）

- 編集作業をする前に必ず実行する

```bash
$ cd kekweb-oh2022
$ git fetch --prune
$ git pull
```

## ファイルを編集する

- 任意のエディターでファイルを編集する
- 特にこだわりがないのであれば `Visual Studio Code`がおすすめ

```shell
$ code /content/post/ファイル名.md
```

## 変更履歴を作成する（`$cz c`）

- ファイルを保存したら、リポジトリに変更履歴を`コミット`する必要がある
- チームでお揃いのコミットメッセージを作成するため`commitizen`を導入した

```bash
$ cz c

# ...
# commitizen のダイアログにしたがって
# 変更した内容を簡単に入力する（日本語OK）
# ...
```

## リポジトリに反映させる（`$ git push`）

- リポジトリにコミットしたあとは、GitLab にアップロードする
- 作業がひと段落した場合は、必ず実行する

```shell
$ git push
```

---

# ローカル環境の構築

- 自分のパソコンの中に、開発環境を構築する
- このセクションの内容は、基本的に、最初に一度だけ実行すれば OK

## 構築ツールをインストールする

- `Git` / `NodeJS` / `commitizen`をインストールする
  - `commitizen`は`Git`操作を補助するツール。もしかしたら`Python`のインストールも必要かも
- すでにこれらのツールがインストールされている場合は、適当にスキップする

```shell
$ brew install git
$ brew install node
$ brew install commitizen  # or pip3 install commitizen
```

## リポジトリをクローンする

- GitLab 上にあるリポジトリを手元にダウンロードする（クローンという）
- SSH 鍵認証の設定が必要かもしれない
  - [GitLab に SSH 鍵を登録する方法の解説はここを参照](https://zenn.dev/shotakaha/articles/ba3e97cfe0837721c3f3)

```shell
$ cd $WORKDIR  # 任意の作業用ディレクトリを用意する
$ git clone git@gitlab.com:kekweb/openhouse2022.git kekweb-oh2022
$ cd kekweb-oh2022
```

## 関連パッケージをインストールする

- ウェブサイトの構築に必要なパッケージをインストールする
- 必要な情報は `package.json` に記載済み
- （ほぼ）何も考えずに `npm install` すれば OK

```shell
$ npm install
```

# 参考リファレンス

- [MDN Docs](https://developer.mozilla.org/ja/) : とりあえず「mdn <HTMLタグ名>」「mdn <CSSプロパティ名>」で検索して使い方を調べる


# 色見本

- ベースのカラーはポスターの色に合わせて ``E05749`` にする
- それと仲がいい色を選ぶようにする
  - https://www.color-site.com/codes/E05749
- プライマリカラー
  - デフォルト : ``E05749`` (R:224 G:87 B:73)
  - 暗め : ``E02E1C`` (R:224 G:46 B:28)
