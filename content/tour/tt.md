+++
title = "全コースの時刻表"
weight = 2
+++


<div class="flex justify-center">
<table class="table-auto">
  <thead class="bg-primary text-xl">
    <tr>
      <th class="px-4 py-2">便名</th>
      <th class="px-4 py-2">Aコース</th>
      <th class="px-4 py-2">Bコース</th>
      <th class="px-4 py-2">Cコース</th>
      <th class="px-4 py-2">Dコース</th>
      <th class="px-4 py-2">Eコース</th>
      <th class="px-4 py-2">Fコース</th>
    </tr>
  </thead>
  <tbody class="text-lg">
<tr>
<td class="px-4 py-2">第1便</td>
<td class="px-4 py-2">09:00 – 10:30</td>
<td class="px-4 py-2">09:00 – 10:30</td>
<td class="px-4 py-2">09:00 – 10:30</td>
<td class="px-4 py-2">09:00 – 10:30</td>
<td class="px-4 py-2">09:00 – 10:30</td>
<td class="px-4 py-2">09:00 – 10:30</td>
</tr>
<tr class="bg-gray-100">
<td class="px-4 py-2">第2便</td>
<td class="px-4 py-2">10:00 – 11:30</td>
<td class="px-4 py-2">09:50 – 11:20</td>
<td class="px-4 py-2">10:00 – 11:30</td>
<td class="px-4 py-2">10:00 – 11:30</td>
<td class="px-4 py-2">10:00 – 11:30</td>
<td class="px-4 py-2">10:00 – 11:30</td>
</tr>
<tr>
<td class="px-4 py-2">第3便</td>
<td class="px-4 py-2">11:00 – 12:30</td>
<td class="px-4 py-2">10:50 – 12:20</td>
<td class="px-4 py-2">11:00 – 12:30</td>
<td class="px-4 py-2">11:00 – 12:30</td>
<td class="px-4 py-2">11:00 – 12:30</td>
<td class="px-4 py-2">11:00 – 12:30</td>
</tr>
<tr class="bg-gray-100">
<td class="px-4 py-2">第4便</td>
<td class="px-4 py-2">12:30 – 14:00</td>
<td class="px-4 py-2">11:40 – 13:10</td>
<td class="px-4 py-2">12:30 – 14:00</td>
<td class="px-4 py-2">12:30 – 14:00</td>
<td class="px-4 py-2">12:30 – 14:00</td>
<td class="px-4 py-2">12:30 – 14:00</td>
</tr>
<tr>
<td class="px-4 py-2">第5便</td>
<td class="px-4 py-2">13:30 – 15:00</td>
<td class="px-4 py-2">12:40 – 14:10</td>
<td class="px-4 py-2">13:30 – 15:00</td>
<td class="px-4 py-2">13:30 – 15:00</td>
<td class="px-4 py-2">13:30 – 15:00</td>
<td class="px-4 py-2">13:30 – 15:00</td>
</tr>
<tr class="bg-gray-100">
<td class="px-4 py-2">第6便</td>
<td class="px-4 py-2">14:30 – 16:00</td>
<td class="px-4 py-2">13:30 – 15:00</td>
<td class="px-4 py-2">14:30 – 16:00</td>
<td class="px-4 py-2">14:30 – 16:00</td>
<td class="px-4 py-2">14:30 – 16:00</td>
<td class="px-4 py-2">14:30 – 16:00</td>
</tr>
<tr>
<td class="px-4 py-2">第7便</td>
<td></td>
<td class="px-4 py-2">14:30 – 16:00</td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>
</div>
