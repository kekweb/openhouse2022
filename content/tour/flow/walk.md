+++
title = "自転車・徒歩でご来場の方"
weight = 13
+++

KEKつくばキャンパスの住所は茨城県つくば市大穂1-1です。
学園東大通りに面した正門までお越しください。
**入って左手側に駐輪場**があります。
学園東大通りは交通量が多いため、通行には十分注意してください。

## 正門から受付まで

正門から各コースの受付まで、看板の案内にしたがって徒歩で移動してください。
受付の場所はコースごとに異なり、6つの建物の中に別々に設置されています。
**正門から受付までは最大で10分程度**かかります。
道に迷ってしまった場合は「STAFF KEKと書かれた赤いワッペン」をつけた係員にお尋ねください。
