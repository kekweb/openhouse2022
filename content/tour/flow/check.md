+++
title = "ご自宅を出発する前の確認事項"
weight = 1
+++

1. 体調が万全であること
1. 予約したツアー（A--Fコース）と便の出発時刻。集合時刻は出発時刻の15分前です（受付は出発時刻の45分前から行います）
1. オンライン予約画面をスマートフォンのアプリで表示できること。アプリで表示できない場合は紙に印刷をお願いします）
1. 感染症対策の誓約書や放射線管理の確認書（詳しくはこちら）をダウンロードしてご持参いただく方は印刷
1. 動きやすい服装・靴であること
