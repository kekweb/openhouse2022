+++
title = "Markdown記法のテスト"
date = "2015-02-20"
categories = ["example", "markdown", "あとで削除する"]
draft = true
+++

ブラウザで Markdown 記法の練習ができる[Markdown Tutorial](http://markdowntutorial.com/)を試してみるといいかもしれません。

---

```md:入力テキスト
**これは太字（ボールド）です**
```

**これは太字（ボールド）です**

---

```md:入力テキスト
_これは斜体（イタリック）です。日本語の文章では基本的に使いません。_
```

_これは斜体（イタリック）です。日本語の文章では基本的に使いません。_

---

```md:入力テキスト
## これはレベル 2 の見出し（`<h2>`）
```

## これはレベル 2 の見出し（`<h2>`）

`#`の数で見出しレベル（`<h1>` から `<h6>`まで）を変更できます

---

表を使うこともできるけど、、、基本的に使わなくていいです。

| Number | Next number | Previous number |
| :----- | :---------- | :-------------- |
| Five   | Six         | Four            |
| Ten    | Eleven      | Nine            |
| Seven  | Eight       | Six             |
| Two    | Three       | One             |

---

画像を挿入することもできます

```md:入力テキスト
![Crepe](http://s3-media3.fl.yelpcdn.com/bphoto/cQ1Yoa75m2yUFFbY2xwuqw/348s.jpg)
```

![Crepe](http://s3-media3.fl.yelpcdn.com/bphoto/cQ1Yoa75m2yUFFbY2xwuqw/348s.jpg)

---

ソースコードのハイライトもできます

```javascript
var foo = function (x) {
  return x + 5;
};
foo(3);
```
