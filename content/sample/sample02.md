+++
author = "Shota Takahashi"
title = "Hugoショートコードの使い方"
date = "2022-06-18"
description = "ショートコードの簡単な使い方"
categories = ["これはカテゴリのサンプル", "あとで削除する"]
tags = ["これはタグのサンプル", "あとで削除する"]
images = []
draft = true
+++

Hugo にはいくつかの[Built-in Shortcodes](https://gohugo.io/content-management/shortcodes/#use-hugos-built-in-shortcodes)がある。
設定ファイル（`config.toml`）で[プライバシーレベルの設定](https://gohugo.io/about/hugo-and-gdpr/) をすることもできる。

<!--more-->

## ビルトインのショートコード

### `figure`

引数
: 1. `src`
  2. `link`
  3. `target`
  4. `rel`
  5. `alt`
  6. `title`
  7. `caption`
  8. `class`
  9. `height`
  10. `width`
  11. `attr`
  12. `attrlink`

### `tweet`

引数
: 1. `user`
  2. `id`

### `youtube`
引数
: 1. `id`
  2. `title`
  3. `autoplay`

## YouTube の埋め込み

KEK 公式 YouTube の動画（https://youtu.be/eZ3ZlLAQCx0）を埋め込むと次のようになる。

{{< youtube eZ3ZlLAQCx0 >}}

## Twitter の埋め込み

KEK 公式 Twitter のツイート（ https://twitter.com/kek_jp/status/1538123920376598529 ）を埋め込むと次のようになる。

{{< twitter user="kek_jp" id="1538123920376598529" >}}
