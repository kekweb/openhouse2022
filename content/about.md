+++
title = "KEK一般公開"
subtitle = "KEK Openhouse"
image = "img/5Q9A3446.JPG"
+++

**KEK 一般公開 2022** のウェブサイトをご覧いただきありがとうございます。

KEK は 1971 年に高エネルギー物理学研究所として設立、その後高エネルギー加速器研究機構に改称、2021 年には 50 周年を迎えました。

本日の一般公開では、普段は見ることのできない実験施設の見学や、科学のふしぎを体感できる工作や実験、世界の研究をリードする研究者の講演とサイエンスカフェなど、小さなお子様から大人の方まで楽しんでいただけるたくさんのプログラムを用意しています。 どうぞごゆっくりお楽しみください。

---

Thank you for visiting a website of KEK Openhouse 2022.

KEK was established as the National Laboratory for High Energy Physics in 1971, then renamed as High Energy Accelerator Research Organization, and marked its 50th anniversary in 2021.

Using accelerators, KEK is conducting research in a wide range of fields, from basic sciences such as the origins of universe, sources of matter and life, to applied sciences that lead to daily life, using researchers from around the world.

Please enjoy !!
