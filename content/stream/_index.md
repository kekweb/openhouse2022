+++
title = "オンライン配信"
pre = '<i class="fas fa-laptop"></i>'
images = ["img/default.jpg"]
+++

## 9月3日（土）10:00 - 15:00

今年のKEK一般公開では「加速器だから見える世界ってどんな世界？」をテーマに5時間のライブ配信を予定しています。

{{< youtube id="aC3gtsJ298U" title="9月3日 10:00 -- 15:00">}}

<div class="text-center">
<p>＼\チャンネル登録もよろしくお願いします/／</p>
</div>

YouTube Live
: https://youtu.be/aC3gtsJ298U

ニコニコ生放送
: https://live.nicovideo.jp/watch/lv338226941




## 配信プログラム
