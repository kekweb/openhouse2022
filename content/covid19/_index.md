+++
title = "感染症対策"
pre = '<i class="fas fa-shield-virus"></i>'
+++

KEK一般公開2022では、下記の感染症対策を実施します。
参加当日までに必ずお読みください。
詳細は[<i class="fas fa-file-pdf"></i> KEK一般公開2022における感染症対策](./countermeasures.pdf)をご確認ください。
